const router = require('express').Router();

router.route('/').get((req, res) => {
    msg = `Hola ${req.query.name || "Mundo"} del request`;
    res.json({msg});
});

router.route('/:lang').get((req, res) => {
    switch(req.params.lang){
        case "es":
            msg = "Hola Mundo";
            break;
        case "en":
            msg = "Hello World";
            break;
        case "fr":
            msg = "Bonjour Monde";
            break;
        default:
            msg = "Your language is not supported";
            break;
    }
    res.json({msg});
});

router.route('/').post((req, res) => {
    msg = `Hola ${req.body.name || "Mundo"} del request post`;
    res.json({msg});
});

module.exports = router;
