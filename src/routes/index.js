const router = require('express').Router();

const helloRoute = require('./hello');
const userRoute = require('./user');

router.use('/hello', helloRoute);
router.use('/user', userRoute);

module.exports = router;
